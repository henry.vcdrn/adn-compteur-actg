﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace TravailAtelierAdn
{    /// <summary>
     /// Projet: ADN 
     /// Details: Atelier Outil du développement
     /// Prenom: Henry.VCDRN
     /// Date: 14.10.2022
     /// Version : 1.0
     /// </summary>
    internal class Program
    {
        static void Main(string[] args)
        {
            StreamReader S;
            S = new StreamReader("G:\\Mon Drive\\Information_SSD\\Classroom\\OF-22-23 Les outils du développeur I.FDA-P1B\\TravailAdn\\chromosome-11-partial\\chromosome-11-partial.txt");
            String line = S.ReadLine();
            int CompterA = 0;
            int CompterT = 0;
            int CompterC = 0;
            int CompterG = 0;
            while (line != null)
            {
                //Console.WriteLine(line);
                for (int i = 0; i < line.Length; i++)
                {
                    if (line[i] == 'A')
                    {
                        CompterA++;
                    }
                    if (line[i] == 'T')
                    {
                        CompterT++;
                    }
                    if (line[i] == 'C')
                    {
                        CompterC++;
                    }
                    if (line[i] == 'G')
                    {
                        CompterG++;
                    }
                    
                }
                line = S.ReadLine();
                //Console.WriteLine(line[0]);

            }
            Console.WriteLine($"Compteur A = {CompterA}");
            Console.WriteLine($"Compteur T = {CompterT}");
            Console.WriteLine($"Compteur C = {CompterC}");
            Console.WriteLine($"Compteur G = {CompterG}");
        }
    }
}
